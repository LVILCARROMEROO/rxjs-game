
import { fromEvent, BehaviorSubject } from 'rxjs';
import { map, scan, tap, withLatestFrom, filter, takeWhile, finalize } from 'rxjs/operators';
import { Coordinate, BoardState } from './interfaces'
import { mouseEventToCoordinates, updateBoardState, checkCell, writeMessage } from './functions'
import { initialBoardState } from './constants';

const board = document.getElementById('board');

const boardState$ = new BehaviorSubject<BoardState>(initialBoardState);
const game$ = fromEvent<MouseEvent>(board, 'click')
.pipe(
  map<MouseEvent, Coordinate>(mouseEventToCoordinates),
  withLatestFrom(boardState$),
  filter(([ coordinate, boardState ]) => !boardState.board[coordinate.row][coordinate.col]),
  tap(([ coordinate, boardState ]) => checkCell(coordinate, boardState.player)),
  map(([ coordinate ]) => coordinate),
  scan<Coordinate, BoardState>(updateBoardState, boardState$.value),
  tap<BoardState>(boardState => boardState$.next(boardState)),
  takeWhile(boardState => !boardState.finished),
  finalize(() => {
    writeMessage(boardState$.value);
    boardState$.complete();
  })
)

game$.subscribe()



