import { Coordinate, BoardState, Player } from './interfaces';

export const mouseEventToCoordinates = (event: MouseEvent): Coordinate => ({
  row: Math.floor(event.clientY / 70),
  col: Math.floor(event.clientX / 70),
})

export const checkCell = (coordinate: Coordinate, player: Player): void => {
  const element = document.getElementById(`${coordinate.row}-${coordinate.col}`);
  element.innerHTML = player;
  element.classList.add(player);
}

export const writeMessage = (boardState: BoardState): void => {
  const message = boardState.winner ? `Ganador ${boardState.winner}` : `Es un empate`; 
  document.getElementById('message').innerHTML = message;
}

export const findWinner = (board: Player[][]): Player => {
  
  const coordinatesWinners: Coordinate[][] = [
    [ { row: 0, col: 0 }, { row: 0, col: 1 }, { row: 0, col: 2 } ],
    [ { row: 1, col: 0 }, { row: 1, col: 1 }, { row: 1, col: 2 } ],
    [ { row: 2, col: 0 }, { row: 2, col: 1 }, { row: 2, col: 2 } ],

    [ { row: 0, col: 0 }, { row: 1, col: 0 }, { row: 2, col: 0 } ],
    [ { row: 0, col: 1 }, { row: 1, col: 1 }, { row: 2, col: 1 } ],
    [ { row: 0, col: 2 }, { row: 1, col: 2 }, { row: 2, col: 2 } ],

    [ { row: 0, col: 0 }, { row: 1, col: 1 }, { row: 2, col: 2 } ],
    [ { row: 0, col: 2 }, { row: 1, col: 1 }, { row: 2, col: 0 } ],
  ]


  for (const coordinates of coordinatesWinners) {
    const coor1 = coordinates[0];
    const coor2 = coordinates[1];
    const coor3 = coordinates[2];

    const value1 = board[coor1.row][coor1.col];
    const value2 = board[coor2.row][coor2.col];
    const value3 = board[coor3.row][coor3.col];

    const isWinner = value1 && (value1 === value2) && (value2 === value3);
    
    if (isWinner)  { return value1; }
  }

  return null;
} 

export const isBoardFull = (board: Player[][]): boolean => {
  
  for( const row of board ) {
    for ( const col of row ) {
      if (!col) return false;   
    }
  }

  return true;

}

export const updateBoardState = (boardState: BoardState, coordinate: Coordinate): BoardState => {

  const board =  boardState.board.map((row: Player[], rowIndex: number) => {
    if (rowIndex === coordinate.row) {
      return row.map((col: Player, colIndex: number) => {
        if (colIndex === coordinate.col) {
          return boardState.player;
        }
        return col;
      })
    }
    return row;
  })

  const player = (boardState.player === Player.O) ? Player.X : Player.O;
  const winner = findWinner(board);
  const finished = isBoardFull(board) || (winner != null);

  return { 
    board, player, finished, winner
  }

}
