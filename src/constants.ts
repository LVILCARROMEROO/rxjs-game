import { BoardState, Player } from './interfaces';

export const initialBoardState: BoardState = {
  board: Array(3).fill('').map(() => Array(3).fill('')),
  player: Player.O,
  finished: false,
  winner: null
}