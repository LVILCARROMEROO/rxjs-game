export enum Player {
  X = 'X',
  O = 'O'
}

export interface Coordinate {
  row: number;
  col: number;
}

export interface BoardState {
  board: Player[][],
  player: Player,
  finished: boolean,
  winner: Player
}